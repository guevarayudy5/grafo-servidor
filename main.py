from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from collections import defaultdict, deque

app = FastAPI()

class Grafo:
    def __init__(self):
        self.grafo = defaultdict(list)

    def agregar_arista(self, u, v):
        self.grafo[u].append(v)
        self.grafo[v].append(u)

    def bfs(self, inicio: int, destino: int):
        visitados = set()
        cola = deque()
        cola.append((inicio, [inicio]))
        while cola:
            nodo, camino = cola.popleft()

            if nodo == destino:
                return {"camino": camino}

            if nodo not in visitados:
                visitados.add(nodo)

                for vecino in self.grafo[nodo]:
                    if vecino not in visitados:
                        nuevo_camino = list(camino)
                        nuevo_camino.append(vecino)
                        cola.append((vecino, nuevo_camino))
        return {"camino": None}

grafo = Grafo()
grafo.agregar_arista(0, 1)
grafo.agregar_arista(0, 3)
grafo.agregar_arista(3, 4)
grafo.agregar_arista(3, 1)
grafo.agregar_arista(4, 5)
grafo.agregar_arista(4, 3)
grafo.agregar_arista(5, 2)
grafo.agregar_arista(0, 2)

class BuscarCaminoRequest(BaseModel):
    inicio: int
    destino: int

@app.post("/buscar_camino")
def buscar_camino(request: BuscarCaminoRequest):
    try:
        resultado = grafo.bfs(request.inicio, request.destino)
        return resultado
    except KeyError:
        raise HTTPException(status_code=400, detail="Nodo no encontrado")

@app.get("/")
def root():
    return {"servicio": "Estructuras de datos con FastAPI"}
